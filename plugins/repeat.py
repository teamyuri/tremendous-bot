from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin

import time

class RepeatPlugin(Plugin):
    def process_message(self, data):
        print("Message data object: {}".format(data))
        if too_old(data):
            print("--- Not processing message - it's too old!")
            return

        if data['text'].lower().find("chuck") >= 0:
            return
            
        self.outputs.append(
            [data['channel'], 'Hello {}, {} to you as well!'.format(
                data['user'], data['text']
            )]
        )

def too_old(data):
    TOO_OLD_THRESHHOLD = 10
    now = int(time.time())
    msg_time = int(data['ts'].split('.')[0])
    if (now - msg_time) > TOO_OLD_THRESHHOLD:
        return True

    return False
