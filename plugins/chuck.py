from __future__ import print_function
from __future__ import unicode_literals

from rtmbot.core import Plugin

import time
import requests

JOKE_URL = "https://api.chucknorris.io/jokes/random?category={}"

class ChuckPlugin(Plugin):
    def process_message(self, data):
        print("Message data object: {}".format(data))
        if too_old(data):
            print("--- Not processing message - it's too old!")
            return
        
        if data['text'].lower().find("chuck") >= 0:
            try:
                category = data['text'].lower().split(" ")[1]
            except Exception as e:
                category = ""  

            joke_url = JOKE_URL.format(category)
            print("====>>>   Joke URL: {}".format(joke_url))
            r = requests.get(joke_url)
            reply = r.json()
            self.outputs.append(
                [data['channel'], "{}. {}".format(reply['value'])]
            )


def too_old(data):
    TOO_OLD_THRESHHOLD = 10
    now = int(time.time())
    msg_time = int(data['ts'].split('.')[0])
    if (now - msg_time) > TOO_OLD_THRESHHOLD:
        return True

    return False
